<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fbuser extends Model
{
    protected $table = 'fbusers';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = ['user_id', 'facebook_user_id', 'email'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
