<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;

class AccountService
{
    public function createOrGetUser(ProviderUser $fbuser)
    {
        $account = Fbuser::where('facebook_user_id', $fbuser->getId())->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new Fbuser([
                'facebook_user_id' => $fbuser->getId()
            ]);
            $user = User::whereEmail($fbuser->getEmail())->first();
            if (!$user) {

                $user = User::create([
                    'email' => $fbuser->getEmail(),
                    'name' => $fbuser->getName(),
                    'password' => time(),
                ]);
            }
            $account->user()->associate($user);
            $account->save();
            return $user;
        }

    }
}