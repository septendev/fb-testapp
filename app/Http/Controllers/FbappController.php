<?php

namespace App\Http\Controllers;

use App\AccountService;
use App\Http\Controllers\Controller;
use App\Fbuser as Fbuser;

use Laravel\Socialite\Two\InvalidStateException;
use Socialize;

class FbappController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | FBappController
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users with facebook API for the
    | application and redirecting them to your home screen.
    |
    */
    /**
     * FB Button login
     *
     * @return void
     */

    public function redirectToProvider()
    {
        return Socialize::with('facebook')->redirect();
    }

    public function callback(AccountService $service)
    {

        $user = $service->createOrGetUser(Socialize::with('facebook')->user());

        auth()->login($user);

        return redirect()->to('/home');

    }
}
